- [serverconfig.txt](https://terraria.fandom.com/wiki/Server#serverconfigfiles)



1. [як створити сервер](https://www.linode.com/docs/guides/host-a-terraria-server-on-your-linode/)
2. відкриваю порт 7777
3. пробую пінгувати порт .\nmap.exe ip -p 7777
4. на VM слухаю порт tcpdump port 443 and '(tcp-syn|tcp-ack)!=0' 
   - [listen port](https://superuser.com/questions/604998/monitor-tcp-traffic-on-specific-port/848966#848966)
5. підключаюсь із телефону
   - порт кажу що приходить щось
     - сервер терарії каже що підключається хтось
       - сервер крешається
